class CreateThorns < ActiveRecord::Migration[5.1]
  def change
    create_table :thorns do |t|
      t.string :name

      t.timestamps
    end
  end
end
