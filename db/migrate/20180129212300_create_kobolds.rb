class CreateKobolds < ActiveRecord::Migration[5.1]
  def change
    create_table :kobolds do |t|
      t.string :name
      t.integer :level
      t.integer :hp

      t.timestamps
    end
  end
end
