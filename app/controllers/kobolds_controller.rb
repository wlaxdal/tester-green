class KoboldsController < ApplicationController
  before_action :set_kobold, only: [:show, :edit, :update, :destroy]

  # GET /kobolds
  # GET /kobolds.json
  def index
    @kobolds = Kobold.all
  end

  # GET /kobolds/1
  # GET /kobolds/1.json
  def show
  end

  # GET /kobolds/new
  def new
    @kobold = Kobold.new
  end

  # GET /kobolds/1/edit
  def edit
  end

  # POST /kobolds
  # POST /kobolds.json
  def create
    @kobold = Kobold.new(kobold_params)

    respond_to do |format|
      if @kobold.save
        format.html { redirect_to @kobold, notice: 'Kobold was successfully created.' }
        format.json { render :show, status: :created, location: @kobold }
      else
        format.html { render :new }
        format.json { render json: @kobold.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /kobolds/1
  # PATCH/PUT /kobolds/1.json
  def update
    respond_to do |format|
      if @kobold.update(kobold_params)
        format.html { redirect_to @kobold, notice: 'Kobold was successfully updated.' }
        format.json { render :show, status: :ok, location: @kobold }
      else
        format.html { render :edit }
        format.json { render json: @kobold.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /kobolds/1
  # DELETE /kobolds/1.json
  def destroy
    @kobold.destroy
    respond_to do |format|
      format.html { redirect_to kobolds_url, notice: 'Kobold was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_kobold
      @kobold = Kobold.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def kobold_params
      params.require(:kobold).permit(:name, :level, :hp)
    end
end
