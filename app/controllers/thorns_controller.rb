class ThornsController < ApplicationController
  before_action :set_thorn, only: [:show, :edit, :update, :destroy]

  # GET /thorns
  # GET /thorns.json
  def index
    @thorns = Thorn.all
  end

  # GET /thorns/1
  # GET /thorns/1.json
  def show
  end

  # GET /thorns/new
  def new
    @thorn = Thorn.new
  end

  # GET /thorns/1/edit
  def edit
  end

  # POST /thorns
  # POST /thorns.json
  def create
    @thorn = Thorn.new(thorn_params)

    respond_to do |format|
      if @thorn.save
        format.html { redirect_to @thorn, notice: 'Thorn was successfully created.' }
        format.json { render :show, status: :created, location: @thorn }
      else
        format.html { render :new }
        format.json { render json: @thorn.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /thorns/1
  # PATCH/PUT /thorns/1.json
  def update
    respond_to do |format|
      if @thorn.update(thorn_params)
        format.html { redirect_to @thorn, notice: 'Thorn was successfully updated.' }
        format.json { render :show, status: :ok, location: @thorn }
      else
        format.html { render :edit }
        format.json { render json: @thorn.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /thorns/1
  # DELETE /thorns/1.json
  def destroy
    @thorn.destroy
    respond_to do |format|
      format.html { redirect_to thorns_url, notice: 'Thorn was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_thorn
      @thorn = Thorn.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def thorn_params
      params.require(:thorn).permit(:name)
    end
end
