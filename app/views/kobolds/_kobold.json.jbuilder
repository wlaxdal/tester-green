json.extract! kobold, :id, :name, :level, :hp, :created_at, :updated_at
json.url kobold_url(kobold, format: :json)
