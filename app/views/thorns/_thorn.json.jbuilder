json.extract! thorn, :id, :name, :created_at, :updated_at
json.url thorn_url(thorn, format: :json)
