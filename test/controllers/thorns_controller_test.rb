require 'test_helper'

class ThornsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @thorn = thorns(:one)
  end

  test "should get index" do
    get thorns_url
    assert_response :success
  end

  test "should get new" do
    get new_thorn_url
    assert_response :success
  end

  test "should create thorn" do
    assert_difference('Thorn.count') do
      post thorns_url, params: { thorn: { name: @thorn.name } }
    end

    assert_redirected_to thorn_url(Thorn.last)
  end

  test "should show thorn" do
    get thorn_url(@thorn)
    assert_response :success
  end

  test "should get edit" do
    get edit_thorn_url(@thorn)
    assert_response :success
  end

  test "should update thorn" do
    patch thorn_url(@thorn), params: { thorn: { name: @thorn.name } }
    assert_redirected_to thorn_url(@thorn)
  end

  test "should destroy thorn" do
    assert_difference('Thorn.count', -1) do
      delete thorn_url(@thorn)
    end

    assert_redirected_to thorns_url
  end
end
