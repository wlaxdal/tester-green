require 'test_helper'

class KoboldsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @kobold = kobolds(:one)
  end

  test "should get index" do
    get kobolds_url
    assert_response :success
  end

  test "should get new" do
    get new_kobold_url
    assert_response :success
  end

  test "should create kobold" do
    assert_difference('Kobold.count') do
      post kobolds_url, params: { kobold: { hp: @kobold.hp, level: @kobold.level, name: @kobold.name } }
    end

    assert_redirected_to kobold_url(Kobold.last)
  end

  test "should show kobold" do
    get kobold_url(@kobold)
    assert_response :success
  end

  test "should get edit" do
    get edit_kobold_url(@kobold)
    assert_response :success
  end

  test "should update kobold" do
    patch kobold_url(@kobold), params: { kobold: { hp: @kobold.hp, level: @kobold.level, name: @kobold.name } }
    assert_redirected_to kobold_url(@kobold)
  end

  test "should destroy kobold" do
    assert_difference('Kobold.count', -1) do
      delete kobold_url(@kobold)
    end

    assert_redirected_to kobolds_url
  end
end
