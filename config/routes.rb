Rails.application.routes.draw do
  
  resources :kobolds
  
  resources :thorns
  # For details on he DSL available within this file, see http://guides.rubyonrails.org/routing.html

   get '/farp', to: 'farp#index'
   get '/gobz', to: 'farp#goblin'
   
  root 'static_pages#index'

end
